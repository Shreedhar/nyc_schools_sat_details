package sampleapp.com.a20180218_sridharreddy_nycschools;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sukumar on 2/18/18.
 */

public class SchoolsListAdapter extends RecyclerView.Adapter<SchoolsListAdapter.SchoolsListHolder> {

    private List<SchoolDetailPojo> mSchoolsList;

    //TODO : Need to avoid getting fragment context into recyclerView adapter.

    private SchoolsListFragment mSchoolsListFragment;

    public SchoolsListAdapter(List<SchoolDetailPojo> passesList, SchoolsListFragment schoolsListFragment) {
        this.mSchoolsListFragment = schoolsListFragment;
        this.mSchoolsList = passesList;
    }

    public void setSchoolsList(List<SchoolDetailPojo> passesList) {
        this.mSchoolsList = passesList;
        notifyDataSetChanged();
    }

    @Override
    public SchoolsListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schools_list_row, parent, false);

        return new SchoolsListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SchoolsListHolder holder, int position) {
        SchoolDetailPojo schoolDetailPojo = mSchoolsList.get(position);


        holder.dbn.setText(schoolDetailPojo.getDbn());
        holder.schoolName.setText(schoolDetailPojo.getSchoolName());
        holder.location.setText(schoolDetailPojo.getLocation());

        holder.dbn.getRootView().setOnClickListener(onClickListener);
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String dbn = ((TextView) v.findViewById(R.id.dbn_number)).getText().toString();
            mSchoolsListFragment.onDetailsClick(dbn);
        }
    };

    @Override
    public int getItemCount() {
        return mSchoolsList.size();
    }


    class SchoolsListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.dbn_number)
        TextView dbn;

        @BindView(R.id.school_name)
        TextView schoolName;

        @BindView(R.id.location)
        TextView location;

        SchoolsListHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
