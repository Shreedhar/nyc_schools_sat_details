package sampleapp.com.a20180218_sridharreddy_nycschools;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getName();

    SchoolsListFragment mSchoolsListFragment;

    SchoolSatDetailsFragment mSchoolSatDetailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*
        Inflating SchoolsListFragment fragment and add to BackStack.
         */
        if (mSchoolsListFragment == null) {
            //TODO : Add to BackStack
            mSchoolsListFragment= new SchoolsListFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction()
                    .add(R.id.schools_inner_content,
                            mSchoolsListFragment,
                            SchoolsListFragment.class.getSimpleName())
                    .addToBackStack(SchoolsListFragment.TAG);
            transaction.commit();
        }


    }

    /*
       Inflating SchoolSatDetailsFragment fragment and add to BackStack.
        */
    public void inflateSchoolDetailsFragment(String dbn){
        mSchoolSatDetailsFragment = new SchoolSatDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SchoolSatDetailsFragment.DBN_NUMBER,dbn);
        mSchoolSatDetailsFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction()
                .add(R.id.schools_inner_content,
                        mSchoolSatDetailsFragment,
                SchoolSatDetailsFragment.class.getSimpleName())
                .addToBackStack(SchoolSatDetailsFragment.TAG);;
        transaction.commit();
    }
}
