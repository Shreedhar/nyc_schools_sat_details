package sampleapp.com.a20180218_sridharreddy_nycschools;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sukumar on 2/18/18.
 */

public class HttpNetworkManager {

    public static final String BASE_URL = "https://data.cityofnewyork.us/resource/";

    public static final int DEFAULT_TIMEOUT = 15;

    private static Retrofit retrofit = null;

    private static HttpNetworkApi httpNetworkApi = null;

    /**
     *
     * @return returns single instance of httpNetworkApi.
     */
    public static HttpNetworkApi getInstance() {
        if (retrofit == null) {

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
            builder.readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
            builder.cache(null);

            retrofit = new Retrofit.Builder()
                    .client(builder.build())
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            httpNetworkApi = retrofit.create(HttpNetworkApi.class);
        }
        return httpNetworkApi;
    }
}
