package sampleapp.com.a20180218_sridharreddy_nycschools;

import android.util.Log;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sukumar on 2/18/18.
 */

public class SchoolsSatDetailsPresenter implements SchoolsListContract.Presenter {

    public static final String TAG = SchoolsSatDetailsPresenter.class.toString();

    HttpNetworkApi mHttpNetworkApi = null;


    SchoolsListContract.View view;

    public SchoolsSatDetailsPresenter(SchoolsListContract.View view ,HttpNetworkApi httpNetworkApi) {

        this.view = view;
        this.view.setPresenter(this);
        mHttpNetworkApi = httpNetworkApi;
    }


    @Override
    public void onFilterClicked() {

    }

    @Override
    public void getSchoolsList() {

    }

    @Override
    public void getSchoolDetails(String dbn) {
        mHttpNetworkApi.getSatScores(dbn).enqueue(new Callback<ArrayList<SchoolSatScorePojo>>() {
            @Override
            public void onResponse(Call<ArrayList<SchoolSatScorePojo>> call, Response<ArrayList<SchoolSatScorePojo>> response) {

                if (response.isSuccessful()) {
                    //TODO : SAT DETAILS API is returning an array instead of Object. Need to check again.

                     ArrayList<SchoolSatScorePojo> schoolSatScorePojoArray = response.body();
                    if(schoolSatScorePojoArray.size()!=0){
                        view.updateSatDetails(schoolSatScorePojoArray.get(0));
                        Log.i(TAG, "get submitted to API." + response.body().toString());
                    }else {
                        view.showErrorMessage("Currently no Schools SAT details are not available with this DBN number");
                    }
                }else {
                    view.showErrorMessage(response.message());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SchoolSatScorePojo>> call, Throwable t) {
                Log.e(TAG, "Unable to submit get to API.");
                view.showErrorMessage("onFailure " + t.getMessage());
            }
        });
    }
}
