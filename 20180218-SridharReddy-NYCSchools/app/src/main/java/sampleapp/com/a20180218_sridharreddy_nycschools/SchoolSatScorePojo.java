package sampleapp.com.a20180218_sridharreddy_nycschools;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sukumar on 2/18/18.
 */

public class SchoolSatScorePojo {

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("num_of_sat_test_takers")
    private String numberOfTestTakers;

    @SerializedName("sat_critical_reading_avg_score")
    private String readingAverageScore;

    @SerializedName("sat_math_avg_score")
    private String mathAverageScrore;

    @SerializedName("sat_writing_avg_score")
    private String writingAverageScrore;

    @SerializedName("school_name")
    private String schoolName;

    public String getDbn() {
        return dbn;
    }

    public String getNumberOfTestTakers() {
        return numberOfTestTakers;
    }

    public String getReadingAverageScore() {
        return readingAverageScore;
    }

    public String getMathAverageScrore() {
        return mathAverageScrore;
    }

    public String getWritingAverageScrore() {
        return writingAverageScrore;
    }

    public String getSchoolName() {
        return schoolName;
    }
}
