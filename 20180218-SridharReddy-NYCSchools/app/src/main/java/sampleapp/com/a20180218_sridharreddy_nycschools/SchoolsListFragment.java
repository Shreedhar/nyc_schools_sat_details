package sampleapp.com.a20180218_sridharreddy_nycschools;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by sukumar on 2/18/18.
 */

public class SchoolsListFragment extends Fragment implements SchoolsListContract.View {

    public static final String TAG = SchoolsListFragment.class.getName();

    private Unbinder unbinder;

    @BindView(R.id.schools_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.filter)
    Button mButton;

    @BindView(R.id.graduate_upper_limit)
    EditText mGraduateUpper;

    @BindView(R.id.graduate_lower_limit)
    EditText mGraduateLower;

    @BindView(R.id.total_students_upper_limit)
    EditText mTotalStudentsUpper;

    @BindView(R.id.total_students_lower_limit)
    EditText mTotalStudentsLower;

    SchoolsListAdapter mAdapter;

    SchoolsListContract.Presenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.schools_list_fragment, container, false);

        unbinder = ButterKnife.bind(this, view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mPresenter = new SchoolsListPresenter(this, HttpNetworkManager.getInstance());

        mButton.setOnClickListener(filterClickListner);

        return view;
    }

    View.OnClickListener filterClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPresenter.onFilterClicked();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        onInitLoad();
    }

    @Override
    public void setPresenter(SchoolsListContract.Presenter presenter) {

    }

    @Override
    public void onInitLoad() {
        mPresenter.getSchoolsList();
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage(int id){
        Toast.makeText(getActivity(), getResources().getString(id), Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateSatDetails(SchoolSatScorePojo schoolSatScorePojo) {

    }

    @Override
    public void updateRecyclerView(List<SchoolDetailPojo> schoolDetailList) {
        if (mRecyclerView.getAdapter() == null) {
            mAdapter = new SchoolsListAdapter(schoolDetailList, this);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setSchoolsList(schoolDetailList);
        }
    }

    @Override
    public String getGradRateUpper() {
        String strText =  mGraduateUpper.getText().toString();
        if(strText.isEmpty()){
            return null;
        }
        return strText;
    }

    @Override
    public String getGradRateLower() {
        String strText =  mGraduateLower.getText().toString();
        if(strText.isEmpty()){
            return null;
        }
        return strText;
    }

    @Override
    public String getTotalStudentsUpper() {
        String strText =  mTotalStudentsUpper.getText().toString();
        if(strText.isEmpty()){
            return null;
        }
        return strText;
    }

    @Override
    public String getTotalStudentsLower() {
        String strText =  mTotalStudentsLower.getText().toString();
        if(strText.isEmpty()){
            return null;
        }
        return strText;
    }

    @Override
    public void showSuccessMessage() {

    }

    public void onDetailsClick(String dbn) {
        ((MainActivity) getActivity()).inflateSchoolDetailsFragment(dbn);
    }
}
