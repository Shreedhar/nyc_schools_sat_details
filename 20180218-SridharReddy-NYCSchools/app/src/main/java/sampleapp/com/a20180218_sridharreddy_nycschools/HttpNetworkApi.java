package sampleapp.com.a20180218_sridharreddy_nycschools;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by sukumar on 2/18/18.
 */

public interface HttpNetworkApi {

    @GET("97mf-9njv.json?")
    Call<ArrayList<SchoolDetailPojo>> getSchoolsList(@Query("$where=graduation_rate<") String graduationRateUpperLimit,
                                                     @Query("$where=graduation_rate>=") String graduationRateLowerLimit,

                                                     @Query("$where=total_students<") String totalStudentsUpperLimit,
                                                     @Query("$where=total_students>=") String totalStudentsLowerLimit);


    @GET("734v-jeq5.json?")
    Call<ArrayList<SchoolSatScorePojo>> getSatScores(@Query("dbn") String dbn);
}
