package sampleapp.com.a20180218_sridharreddy_nycschools;

import java.util.List;

/**
 * Created by sukumar on 2/18/18.
 */

public class SchoolsListContract {

    interface View {
        public void setPresenter(Presenter presenter);

        public void onInitLoad();

        public void showErrorMessage(String errorMessage);

        public void showErrorMessage(int stringId);

        public void  updateSatDetails(SchoolSatScorePojo  schoolSatScorePojo);

        public void updateRecyclerView(List<SchoolDetailPojo> schoolDetailList);

        public String getGradRateUpper();

        public String getGradRateLower();

        public String getTotalStudentsUpper();

        public String getTotalStudentsLower();

        public void showSuccessMessage();



    }

    public interface Presenter {

        public void onFilterClicked();

        public void getSchoolsList();

        public void getSchoolDetails(String dbn);
    }
}
