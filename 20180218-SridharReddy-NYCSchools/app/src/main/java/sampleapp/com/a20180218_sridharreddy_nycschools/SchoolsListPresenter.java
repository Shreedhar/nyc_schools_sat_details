package sampleapp.com.a20180218_sridharreddy_nycschools;

import android.util.Log;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sukumar on 2/18/18.
 */

public class SchoolsListPresenter implements SchoolsListContract.Presenter {

    public static final String TAG = SchoolsListPresenter.class.toString();

    HttpNetworkApi mHttpNetworkApi = null;


    SchoolsListContract.View view;


    public SchoolsListPresenter(SchoolsListContract.View view, HttpNetworkApi httpNetworkApi) {
        this.view = view;
        this.view.setPresenter(this);
        mHttpNetworkApi = httpNetworkApi;
    }


    @Override
    public void getSchoolsList() {
        fetchSchoolsList(null, null, null, null);
    }

    @Override
    public void onFilterClicked() {
        if (view.getGradRateUpper()!=null&&
                view.getGradRateUpper()!=null&&
                view.getTotalStudentsUpper()!=null &&
                view.getTotalStudentsLower()!=null) {
            view.showErrorMessage(R.string.error_message_for_no_field_entry);
            return;
        }
        fetchSchoolsList(view.getGradRateUpper(), view.getGradRateUpper(), view.getTotalStudentsUpper(), view.getTotalStudentsLower());

    }

    /* Makes HTTP call via Retrofit.
     */
    private void fetchSchoolsList(String gradUpper, String gradLower, String studentsUpper, String studentsLower) {
        mHttpNetworkApi.getSchoolsList(gradUpper, gradLower, studentsUpper, studentsLower)
                .enqueue(new Callback<ArrayList<SchoolDetailPojo>>() {
            @Override
            public void onResponse(Call<ArrayList<SchoolDetailPojo>> call, Response<ArrayList<SchoolDetailPojo>> response) {

                if (response.isSuccessful()) {
                    view.updateRecyclerView(response.body());
                    view.showSuccessMessage();
                    Log.i(TAG, "get submitted to API." + response.body().toString());
                }else {
                    view.showErrorMessage(response.message());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SchoolDetailPojo>> call, Throwable t) {
                Log.e(TAG, "Unable to submit get to API.");
                view.showErrorMessage("onFailure " + t.getMessage());
            }
        });
    }

    @Override
    public void getSchoolDetails(String dbn) {

    }
}
