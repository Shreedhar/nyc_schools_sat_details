package sampleapp.com.a20180218_sridharreddy_nycschools;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sukumar on 2/18/18.
 */

public class SchoolDetailPojo {

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("location")
    private String location;

    public String getSchoolName() {
        return schoolName;
    }


    public String getDbn() {
        return dbn;
    }


    public String getLocation() {
        return location;
    }

}
