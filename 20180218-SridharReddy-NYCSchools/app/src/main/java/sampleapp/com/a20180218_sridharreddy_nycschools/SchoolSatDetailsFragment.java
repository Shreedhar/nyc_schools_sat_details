package sampleapp.com.a20180218_sridharreddy_nycschools;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by sukumar on 2/18/18.
 */

public class SchoolSatDetailsFragment extends Fragment implements SchoolsListContract.View {

    private Unbinder unbinder;

    public static final String TAG = SchoolsListFragment.class.getName();

    public static final String DBN_NUMBER = "dbn";

    SchoolsListContract.Presenter mPresenter;

    @BindView(R.id.dbn)
    TextView mDbn;

    @BindView(R.id.test_takers)
    TextView mTestTakers;

    @BindView(R.id.math_average)
    TextView mMathAverageScrore;

    @BindView(R.id.reading_average)
    TextView mReadingAverageScore;

    @BindView(R.id.school_name)
    TextView mSchoolName;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.school_sat_detail_fragment, container, false);

        unbinder = ButterKnife.bind(this, view);

        mPresenter = new SchoolsSatDetailsPresenter(this, HttpNetworkManager.getInstance());

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        String dbn = getArguments().getString(DBN_NUMBER);
        if (dbn != null && !dbn.isEmpty()) {
            mPresenter.getSchoolDetails(dbn);
        }
    }

    @Override
    public void setPresenter(SchoolsListContract.Presenter presenter) {

    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Toast.makeText(getActivity(),errorMessage,Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage(int stringId) {
        Toast.makeText(getActivity(),getResources().getString(stringId),Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateSatDetails(SchoolSatScorePojo schoolSatScorePojo) {

        mDbn.setText(schoolSatScorePojo.getDbn());

        mTestTakers.setText(schoolSatScorePojo.getNumberOfTestTakers());

        mMathAverageScrore.setText(schoolSatScorePojo.getMathAverageScrore());

        mReadingAverageScore.setText(schoolSatScorePojo.getReadingAverageScore());

        mSchoolName.setText(schoolSatScorePojo.getSchoolName());

    }

    @Override
    public void updateRecyclerView(List<SchoolDetailPojo> schoolDetailList) {

    }

    @Override
    public String getGradRateUpper() {
        return null;
    }

    @Override
    public String getGradRateLower() {
        return null;
    }

    @Override
    public String getTotalStudentsUpper() {
        return null;
    }

    @Override
    public String getTotalStudentsLower() {
        return null;
    }

    @Override
    public void showSuccessMessage() {

    }

    @Override
    public void onInitLoad() {

    }
}
