package sampleapp.com.a20180218_sridharreddy_nycschools;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by sukumar on 2/19/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class SchoolsListPresenterTest {

    @Mock
    private SchoolsListContract.View view;

    @Mock
    private HttpNetworkApi service;

    SchoolsListPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new SchoolsListPresenter(view, service);
    }

    /*
    should show success message upon successful data fetch.
     */
    @Test
    public void getInitLoadSchoolsList() throws Exception {


        when(service.getSchoolsList(null,null,null,null)).thenReturn(call);

        presenter.getSchoolsList();

        verify(view).showSuccessMessage();

    }

    /*
    should show error when filter button pressed but no fields are entered.
     */
    @Test
    public void showErrorOnFilter() throws Exception {

        when(view.getGradRateUpper()).thenReturn("");
        when(view.getGradRateLower()).thenReturn("");
        when(view.getTotalStudentsUpper()).thenReturn("");
        when(view.getTotalStudentsLower()).thenReturn("");
        presenter.onFilterClicked();
        verify(view).showErrorMessage(R.string.error_message_for_no_field_entry);
    }


    @Test
    public void getSchoolDetails() throws Exception {

    }


    Call<ArrayList<SchoolDetailPojo>> call = new Call<ArrayList<SchoolDetailPojo>>() {
        @Override
        public Response<ArrayList<SchoolDetailPojo>> execute() throws IOException {

            return null;
        }

        @Override
        public void enqueue(Callback<ArrayList<SchoolDetailPojo>> callback) {

        }

        @Override
        public boolean isExecuted() {
            return false;
        }

        @Override
        public void cancel() {

        }

        @Override
        public boolean isCanceled() {
            return false;
        }

        @Override
        public Call<ArrayList<SchoolDetailPojo>> clone() {
            return null;
        }

        @Override
        public Request request() {
            return null;
        }
    };
}