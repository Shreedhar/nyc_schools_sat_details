Project details & summary :

	This project shows list of New York schools list with their dan number and location. These list can be filtered by graduation rate or total number of students. Upon individual row click, all the SAT details will be displayed.

1. This project using MVP architecture with third party libraries - Retrofit2, ButterKnife, Mockito, Junit , Gson and used Singleton Design pattern for Network classes.
2. Written Test cases for Presentation layer using Mockito and Junit 4. But Mocking the response from Retrofit2 seems difficult. (If more time is provided I would have done proper test cases.)
3. Added Filter feature to the application, so that user can filter NewYork schools based upon Graduation Rate and Total number of students.

Things I might have done if more time is provided.

1. proper mocking of retrofit response for Unit testing.
2. integrate RxAndroid to communicate between Model layer and presenter layer.
3. Would have handled orientation changes more efficiently. Currently application screen orientation is locked to portrait mode.